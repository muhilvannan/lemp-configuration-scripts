#!/bin/bash

if pidof mysqld; then
	echo -n $(date)>> /var/log/mysql_checker.log
	echo '	mysql running '>> /var/log/mysql_checker.log
else
	echo -n $(date)>> /var/log/mysql_checker.log
	echo '	mysql not running' >> /var/log/mysql_checker.log
	service mysqld start
		if pidof mysqld; then
			echo -n $(date) >> /var/log/mysql_checker.log
			echo '	mysqld running after restart'>> /var/log/mysql_checker.log

			echo '	mysqld restarted on server' | mail -s 'mysql crashed and restarted' muhil@accentdesign.co.uk
		else
			echo -n $(date)>> /var/log/mysql_checker.log
			echo '	mysqld crashed -- please reboot'>> /var/log/mysql_checker.log

			echo  'COULD NOT RESTART MYSQL. PLEASE RESTART MYSQL' | mail -s 'MYSQL CRASH' muhil@accentdesign.co.uk
		fi
fi
