#!/bin/bash

RESULT=`ps -a | sed -n /mysqld/p`

if [ "${RESULT:-null}" = null ]; then
    # install percona
    yum -y install http://www.percona.com/downloads/percona-release/percona-release-0.0-1.x86_64.rpm
    yum -y install Percona-Server-client-55 Percona-Server-server-55
    
    echo 'percona installed'
    
    chkconfig mysql on
    service mysql start
    
    echo 'percona started'
else
    echo "mysql installed / running please remove / stop before continuing"
fi