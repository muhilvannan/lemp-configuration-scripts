#!/bin/bash

echo "Type the number of process threads you need nginx configured for"

read processThreads

cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak

echo 'created nginx configuration file backup'

echo "
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user  nginx;
worker_processes  $processThreads;

error_log  /var/log/nginx/error.log;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '\$remote_addr - \$remote_user [\$time_local] \"\$request\" '
                      '\$status \$body_bytes_sent \"\$http_referer\" '
                      '\"\$http_user_agent\" \"\$http_x_forwarded_for\"';

    access_log  /var/log/nginx/access.log  main;
    sendfile        on;
    #tcp_nopush     on;
    keepalive_timeout  65;
    gzip  on;
    include /etc/nginx/conf.d/*.conf;
}

" > /etc/nginx/nginx.conf

echo 'created custom nginx configuration file'

RESULT=`ps -a | sed -n /nginx/p`

if [ "${RESULT:-null}" = null ]; then
    service nginx start
    echo "nginx started"
else
    service nginx restart
    echo "nginx reloaded"
fi

