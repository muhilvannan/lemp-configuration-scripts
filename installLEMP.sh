#!/bin/bash
# LEMP App server install
# install php nginx memcache varnish and php-fpm

if [ ! -f /etc/yum.repos.d/epel.repo ]; then
	wget http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
	rpm -Uvh epel-release-6*.rpm
	rm -f epel*

fi

if [ ! -f /etc/yum.repos.d/remi.repo ]; then
	wget http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
	rpm -Uvh remi-release-6*.rpm
	rm -f remi*
fi


yum -y install nginx php php-common php-fpm php-devel php-gd php-imap php-ldap php-magpierss php-mbstring php-mysql php-odbc php-pdo php-pear php-pecl-apc php-pecl-memcache php-snmp php-soap php-tidy php-xml php-xmlrpc php-zts varnish memcached perl-Cache-Memcached

echo 'nginx php-fpm installed'


# switch off apache
RESULT=`ps -a | sed -n /httpd/p`

if [ "${RESULT:-null}" = null ]; then
    
    echo 'apache stopped already'

else
    service httpd stop
    chkconfig httpd off
fi

# load lemp services on boot
chkconfig nginx on
chkconfig php-fpm on
chkconfig memcached on
#chkconfig varnish on


# start lemp services
service nginx start
#service varrnish start
service memcached start
service php-fpm start 
