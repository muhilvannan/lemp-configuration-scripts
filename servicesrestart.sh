#!/bin/bash
echo 'restarting services ....'
service mysql restart
service nginx restart
#service varnish restart
service php-fpm restart
service memcached restart
echo 'done'
